# `maplec`

Maple is a statically typed, purely functional language that is essesntially a subset of Haskell.

`maplec` is an optimizing compiler for Maple.

This project explores various optimizations / static analysis used by Haskell and other popular programming languages.

## Features

1. type inference
1. pattern matching
    1. redundant pattern detection
        - ```haskell
          -- errors because the third pattern is redundant
          head :: [a] -> Option a
          head []       = None
          head (x : _)  = Some x
          head (x : xs) = Some x
          ```
    1. non-exhaustive pattern detection
        - ```haskell
          -- errors because we are not considering the pattern `[]`
          head :: [a] -> Option a
          head (x : _) = Some x
          ```
1. generic types
    - ```haskell
      head :: [a] -> Option a
      head []      = None
      head (x : _) = Some x
      ```
