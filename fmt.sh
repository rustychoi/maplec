#!/usr/bin/env bash

declare -r ROOT=$(git rev-parse --show-toplevel)

find "$ROOT/src/" "$ROOT/test/" -type f -name '*.hs' | 
    xargs brittany --write-mode=inplace --config-file "$ROOT/brittany.conf"
