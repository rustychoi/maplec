module Type.TypeInference
    ( typeInfer
    ) where

import           Type.Internal.AST2TypedAST     ( prog2TypedProgram )
import           Type.TypedAST                  ( TExpr(..)
                                                , TypedConstructor(..)
                                                , TypedDeconstructor(..)
                                                , TypedExpr(..)
                                                , TypedProgram(..)
                                                , TypedStatement(..)
                                                , TypedTypeDeclaration(..)
                                                )
import           AST                            ( Expr(..)
                                                , Constructor(..)
                                                , Deconstructor(..)
                                                , Prog
                                                , Statement(..)
                                                )
import           Type.Internal.ConstraintSolver ( solveConstraints
                                                , Substitution(..)
                                                , applySubstitution
                                                )
import           Type.Internal.Constraint       ( constraints
                                                , Constraint(..)
                                                )
import           Type.Internal.Util             ( reduce )

-- TODO
typeInfer :: Prog -> TypedProgram
typeInfer p =
    let -- assign uniq types to `p`
        (typedProgram        , progI) = prog2TypedProgram p
        -- generate constraints from `p`
        (constraintsInProgram, csI  ) = constraints typedProgram progI
        substitutions                 = solveConstraints constraintsInProgram
        --
    in  applyAllSubstitutions substitutions typedProgram  where
        --
    applyAllSubstitutions :: [Substitution] -> TypedProgram -> TypedProgram
    applyAllSubstitutions []       tp = tp
    applyAllSubstitutions (s : ss) tp = applyAllSubstitutions ss $ subProgram s tp

{-
--
data TExpr = TVar String
           | TConstr String [TExpr]
           | TFunc TExpr TExpr
           | TList TExpr
           | TParam String
           | TPrim TPrimitive
--
data TPrimitive = PrimInt
                | PrimBool
                | PrimStr
-}

subProgram :: Substitution -> TypedProgram -> TypedProgram
subProgram s (TypedProg stmts expr) = TypedProg subbedStatements subbedExpr  where
    subbedStatements = map (subStatement s) stmts
    subbedExpr       = subExpr s expr

subTypeDeclaration :: Substitution -> TypedTypeDeclaration -> TypedTypeDeclaration
subTypeDeclaration _ _ = error "todo"

subStatement :: Substitution -> TypedStatement -> TypedStatement
subStatement _ _ = error "todo"

subConstructor :: Substitution -> TypedConstructor -> TypedConstructor
subConstructor s (TypedConstructValue tExpr name        ) = TypedConstructValue (applySubstitution s tExpr) name
subConstructor s (TypedConstructListNil tExpr           ) = TypedConstructListNil $ applySubstitution s tExpr
subConstructor s (TypedConstructListCons tExpr arg1 arg2) = TypedConstructListCons subbedTExpr subbedArg1 subbedArg2  where
    subbedTExpr = applySubstitution s tExpr
    subbedArg1  = subConstructor s arg1
    subbedArg2  = subConstructor s arg2
subConstructor s (TypedConstructExpr expr             ) = TypedConstructExpr $ subExpr s expr
subConstructor s (TypedConstructData tExpr name fields) = TypedConstructData subbedTExpr name subbedFields  where
    subbedTExpr  = applySubstitution s tExpr
    subbedFields = map (subConstructor s) fields
subConstructor s (TypedConstructInt  n  ) = TypedConstructInt n
subConstructor s (TypedConstructBool b  ) = TypedConstructBool b
subConstructor s (TypedConstructStr  str) = TypedConstructStr str

subDeconstructor :: Substitution -> TypedDeconstructor -> TypedDeconstructor
subDeconstructor s (TypedDeconstructValue tExpr name        ) = TypedDeconstructValue (applySubstitution s tExpr) name
subDeconstructor s (TypedDeconstructListCons tExpr arg1 arg2) = TypedDeconstructListCons subbedTExpr subbedArg1 subbedArg2  where
    subbedTExpr = applySubstitution s tExpr
    subbedArg1  = subDeconstructor s arg1
    subbedArg2  = subDeconstructor s arg2
subDeconstructor s (TypedDeconstructListNil tExpr         ) = TypedDeconstructListNil $ applySubstitution s tExpr
subDeconstructor s (TypedDeconstructBool    b             ) = TypedDeconstructBool b
subDeconstructor s (TypedDeconstructInt     n             ) = TypedDeconstructInt n
subDeconstructor s (TypedDeconstructStr     str           ) = TypedDeconstructStr str
subDeconstructor s (TypedDeconstructData tExpr name fields) = error ""

subExpr :: Substitution -> TypedExpr -> TypedExpr
subExpr s (TypedAdd lhs rhs                     ) = subBinaryExpr s (TypedAdd) lhs rhs
subExpr s (TypedSub lhs rhs                     ) = subBinaryExpr s (TypedSub) lhs rhs
subExpr s (TypedMul lhs rhs                     ) = subBinaryExpr s (TypedMul) lhs rhs
subExpr s (TypedDiv lhs rhs                     ) = subBinaryExpr s (TypedDiv) lhs rhs
subExpr s (TypedAnd lhs rhs                     ) = subBinaryExpr s (TypedAnd) lhs rhs
subExpr s (TypedOr  lhs rhs                     ) = subBinaryExpr s (TypedOr) lhs rhs
subExpr s (TypedEq  lhs rhs                     ) = subBinaryExpr s (TypedEq) lhs rhs
subExpr s (TypedNeq lhs rhs                     ) = subBinaryExpr s (TypedNeq) lhs rhs
subExpr s (TypedGt  lhs rhs                     ) = subBinaryExpr s (TypedGt) lhs rhs
subExpr s (TypedMod lhs rhs                     ) = subBinaryExpr s (TypedMod) lhs rhs
subExpr s (TypedGe  lhs rhs                     ) = subBinaryExpr s (TypedGe) lhs rhs
subExpr s (TypedLt  lhs rhs                     ) = subBinaryExpr s (TypedLt) lhs rhs
subExpr s (TypedLe  lhs rhs                     ) = subBinaryExpr s (TypedLe) lhs rhs
subExpr s (TypedNegate expr                     ) = TypedNegate $ subExpr s expr
subExpr s (TypedNot    expr                     ) = TypedNot $ subExpr s expr
subExpr s (TypedIfElse tExpr cond ifTrue ifFalse) = TypedIfElse subbedTExpr subbedCond subbedIfTrue subbedIfFalse  where
    subbedTExpr   = applySubstitution s tExpr
    subbedCond    = subExpr s cond
    subbedIfTrue  = subExpr s ifTrue
    subbedIfFalse = subExpr s ifFalse
subExpr s (TypedCall tExpr func arg) = TypedCall subbedTExpr subbedFunc subbedArg  where
    subbedTExpr = applySubstitution s tExpr
    subbedFunc  = subExpr s func
    subbedArg   = subExpr s arg
subExpr s (TypedLambda tExpr arg body) = TypedLambda subbedTExpr subbedArg subbedBody  where
    subbedTExpr = applySubstitution s tExpr
    subbedArg   = subDeconstructor s arg
    subbedBody  = subExpr s body
subExpr s (TypedLetIn tExpr stmts body) = TypedLetIn subbedTExpr subbedStatements subbedBody  where
    subbedTExpr      = applySubstitution s tExpr
    subbedStatements = map (subStatement s) stmts
    subbedBody       = subExpr s body
subExpr s (TypedConstr c                ) = TypedConstr $ subConstructor s c
subExpr s (TypedMatch tExpr arg arm arms) = TypedMatch subbedTExpr subbedArg subbedArm subbedArms  where
    subbedTExpr              = applySubstitution s tExpr
    subbedArg                = subExpr s arg
    (subbedArm : subbedArms) = map (\(pat, body) -> (subDeconstructor s pat, subExpr s body)) (arm : arms)

subBinaryExpr :: Substitution -> (TypedExpr -> TypedExpr -> TypedExpr) -> TypedExpr -> TypedExpr -> TypedExpr
subBinaryExpr s constructor lhs rhs = constructor (subExpr s lhs) (subExpr s rhs)

{-
--
data TypedProgram = TypedProg [TypedStatement] TypedExpr
--
data TypedTypeDeclaration = TypedFuncTypeDeclaration TExpr String TypedTypeDeclaration [TypedTypeDeclaration]
                          | TypedDataTypeDeclaration TExpr String [TypedTypeDeclaration]
--
data TypedStatement = TypedDecl TExpr String [TypedDeconstructor] TypedExpr
               | TypedTypeDecl TypedTypeDeclaration
               | TypedDataDecl TExpr String [String] (String, [String]) [(String, [String])]
--
data TypedConstructor = TypedConstructInt Int
                      | TypedConstructBool Bool
                      | TypedConstructStr String
                      | TypedConstructChar Char
                      | TypedConstructValue TExpr String
                      | TypedConstructListNil TExpr
                      | TypedConstructListCons TExpr TypedConstructor TypedConstructor
                      | TypedConstructExpr TypedExpr
                      | TypedConstructData TExpr String [TypedConstructor]
--
data TypedDeconstructor = TypedDeconstructValue TExpr String
                        | TypedDeconstructListCons TExpr TypedDeconstructor TypedDeconstructor
                        | TypedDeconstructListNil TExpr
                        | TypedDeconstructBool Bool
                        | TypedDeconstructInt Int
                        | TypedDeconstructStr String
                        | TypedDeconstructChar Char
                        | TypedDeconstructData TExpr String [TypedDeconstructor]
--
data TypedExpr = TypedAdd TypedExpr TypedExpr
               | TypedSub TypedExpr TypedExpr
               | TypedMul TypedExpr TypedExpr
               | TypedDiv TypedExpr TypedExpr
               | TypedNegate TypedExpr
               | TypedAnd TypedExpr TypedExpr
               | TypedOr TypedExpr TypedExpr
               | TypedEq TypedExpr TypedExpr
               | TypedNeq TypedExpr TypedExpr
               | TypedGt TypedExpr TypedExpr
               | TypedGe TypedExpr TypedExpr
               | TypedLt TypedExpr TypedExpr
               | TypedLe TypedExpr TypedExpr
               | TypedNot TypedExpr
               | TypedIfElse TExpr TypedExpr TypedExpr TypedExpr
               | TypedCall TExpr TypedExpr [TypedExpr]
               | TypedLambda TExpr TypedDeconstructor TypedExpr
               | TypedLetIn TExpr [TypedStatement] TypedExpr
               | TypedConstr TypedConstructor
               | TypedMatch TExpr TypedExpr (TypedDeconstructor, TypedExpr) [(TypedDeconstructor, TypedExpr)]

--
data TExpr = TVar String
           | TConstr String [TExpr]
           | TFunc TExpr TExpr
           | TList TExpr
           | TParam String
           | TPrim TPrimitive
--
data TPrimitive = PrimInt
                | PrimBool
                | PrimStr
                | PrimChar
-}
