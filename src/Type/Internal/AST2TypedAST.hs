module Type.Internal.AST2TypedAST
    ( prog2TypedProgram
    ) where

import           Type.Internal.Util             ( reduce
                                                , generateTVar
                                                , generateTConstr
                                                , generateTFunc
                                                , generateTList
                                                )
import           Type.TypedAST                  ( TExpr(..)
                                                , TypedConstructor(..)
                                                , TypedDeconstructor(..)
                                                , TypedTypeDeclaration(..)
                                                , TypedExpr(..)
                                                , TypedProgram(..)
                                                , TPrimitive(..)
                                                , TypedStatement(..)
                                                )
import           AST                            ( Prog(..)
                                                , Expr(..)
                                                , Statement(..)
                                                , TypeDeclaration(..)
                                                , Constructor(..)
                                                , Deconstructor(..)
                                                )
--
prog2TypedProgram :: Prog -> (TypedProgram, Int)
prog2TypedProgram (Program stmts expr) =
    let (tStatements, stmtI) = giveUniqTypes2Each giveUniqTypes2Statement 0 stmts
        (tExpr      , exprI) = giveUniqTypes2Expr stmtI expr
    in  (TypedProg tStatements tExpr, exprI)

--
toTypedBinaryExpr :: Int -> (TypedExpr -> TypedExpr -> TypedExpr) -> Expr -> Expr -> (TypedExpr, Int)
toTypedBinaryExpr i f lhs rhs =
    let (typedLHS, lhsI) = giveUniqTypes2Expr i lhs
        (typedRHS, rhsI) = giveUniqTypes2Expr lhsI rhs
    in  (f typedLHS typedRHS, rhsI)

-- # Parameters
--
-- 1. `f :: Int -> a -> (b, Int)`
-- 2. `i :: Int`
-- 3. `xs:: [a]`
--
-- # Returns
--
-- `(ys, ti) :: ([b], Int)`, where
giveUniqTypes2Each :: (Int -> a -> (b, Int)) -> Int -> [a] -> ([b], Int)
giveUniqTypes2Each _ i [] = ([], i)
giveUniqTypes2Each f i (x : xs) =
    let (tx , i0) = f i x
        (txs, i1) = giveUniqTypes2Each f i0 xs
    in  (tx : txs, i1)

--
giveUniqTypes2Expr :: Int -> Expr -> (TypedExpr, Int)
giveUniqTypes2Expr i (Add lhs rhs) = toTypedBinaryExpr i (TypedAdd) lhs rhs
giveUniqTypes2Expr i (Sub lhs rhs) = toTypedBinaryExpr i (TypedSub) lhs rhs
giveUniqTypes2Expr i (Mul lhs rhs) = toTypedBinaryExpr i (TypedMul) lhs rhs
giveUniqTypes2Expr i (Div lhs rhs) = toTypedBinaryExpr i (TypedDiv) lhs rhs
giveUniqTypes2Expr i (And lhs rhs) = toTypedBinaryExpr i (TypedAnd) lhs rhs
giveUniqTypes2Expr i (Or  lhs rhs) = toTypedBinaryExpr i (TypedOr) lhs rhs
giveUniqTypes2Expr i (Eq  lhs rhs) = toTypedBinaryExpr i (TypedEq) lhs rhs
giveUniqTypes2Expr i (Neq lhs rhs) = toTypedBinaryExpr i (TypedNeq) lhs rhs
giveUniqTypes2Expr i (Gt  lhs rhs) = toTypedBinaryExpr i (TypedGt) lhs rhs
giveUniqTypes2Expr i (Mod lhs rhs) = toTypedBinaryExpr i (TypedMod) lhs rhs
giveUniqTypes2Expr i (Ge  lhs rhs) = toTypedBinaryExpr i (TypedGe) lhs rhs
giveUniqTypes2Expr i (Lt  lhs rhs) = toTypedBinaryExpr i (TypedLt) lhs rhs
giveUniqTypes2Expr i (Le  lhs rhs) = toTypedBinaryExpr i (TypedLe) lhs rhs
giveUniqTypes2Expr i (Negate expr) = let (typedExpr, newI) = giveUniqTypes2Expr i expr in (TypedNegate typedExpr, newI)
giveUniqTypes2Expr i (Not    expr) = let (typedExpr, newI) = giveUniqTypes2Expr i expr in (TypedNot typedExpr, newI)
giveUniqTypes2Expr i (IfElse cond ifTrue ifFalse) =
    let (tIfElse     , ifElseI) = generateTVar i
        (typedCond   , condI  ) = giveUniqTypes2Expr ifElseI cond
        (typedIfTrue , trueI  ) = giveUniqTypes2Expr condI ifTrue
        (typedIfFalse, falseI ) = giveUniqTypes2Expr trueI ifFalse
    in  (TypedIfElse tIfElse typedCond typedIfTrue typedIfFalse, falseI)
giveUniqTypes2Expr i (Call func arg) =
    let (tCall    , callI) = generateTVar i
        (typedFunc, funcI) = giveUniqTypes2Expr callI func
        (typedArg , targI) = giveUniqTypes2Expr funcI arg
    in  (TypedCall tCall typedFunc typedArg, targI)
giveUniqTypes2Expr i (Lambda arg body) =
    let (tLambda, lambdaI) = generateTFunc i
        (targ   , argI   ) = giveUniqTypes2Deconstr lambdaI arg
        (tbody  , bodyI  ) = giveUniqTypes2Expr argI body
    in  (TypedLambda tLambda targ tbody, bodyI)
giveUniqTypes2Expr i (LetIn stmts expr) =
    let (tLetIn         , letInI) = generateTVar i
        (typedStatements, stmtI ) = giveUniqTypes2Each giveUniqTypes2Statement letInI stmts
        (typedExpr      , exprI ) = giveUniqTypes2Expr stmtI expr
    in  (TypedLetIn tLetIn typedStatements typedExpr, exprI)
giveUniqTypes2Expr i (Constr c) = let (tc, ti) = giveUniqTypes2Constr i c in (TypedConstr tc, ti)
giveUniqTypes2Expr i (Match expr arm1 arms) =
    let (tMatch   , matchI)        = generateTVar i
        (typedExpr, exprI )        = giveUniqTypes2Expr matchI expr
        --
        allArms                    = arm1 : arms
        (tDeconstrs, decI )        = giveUniqTypes2Each giveUniqTypes2Deconstr exprI (map fst allArms)
        (tBodies   , bodyI)        = giveUniqTypes2Each giveUniqTypes2Expr decI (map snd allArms)
        (typedArm : typedArmsRest) = zip tDeconstrs tBodies
    in  (TypedMatch tMatch typedExpr typedArm typedArmsRest, bodyI)

--
giveUniqTypes2Constr :: Int -> Constructor -> (TypedConstructor, Int)
giveUniqTypes2Constr i (ConstructInt   n) = (TypedConstructInt n, i)
giveUniqTypes2Constr i (ConstructBool  b) = (TypedConstructBool b, i)
giveUniqTypes2Constr i (ConstructStr   s) = (TypedConstructStr s, i)
giveUniqTypes2Constr i (ConstructValue x) = let (tValue, valueI) = generateTVar i in (TypedConstructValue tValue x, valueI)
giveUniqTypes2Constr i ConstructListNil   = let (tList, listI) = generateTList i in (TypedConstructListNil tList, listI)
giveUniqTypes2Constr i (ConstructListCons x1 x2) =
    let (tList   , listI  ) = generateTList i
        (tConstrs, constrI) = giveUniqTypes2Each giveUniqTypes2Constr listI (x1 : [x2])
        (tx1 : tx2 : [])    = tConstrs
    in  (TypedConstructListCons tList tx1 tx2, constrI)
giveUniqTypes2Constr i (ConstructExpr expr) = let (tExpr, ti) = giveUniqTypes2Expr i expr in (TypedConstructExpr tExpr, ti)
giveUniqTypes2Constr i (ConstructData name xs) =
    let (tExpr   , exprI  ) = generateTVar i
        (tConstrs, constrI) = giveUniqTypes2Each giveUniqTypes2Constr exprI xs
    in  (TypedConstructData tExpr name tConstrs, constrI)

--
giveUniqTypes2Deconstr :: Int -> Deconstructor -> (TypedDeconstructor, Int)
giveUniqTypes2Deconstr i (DeconstructValue s) =
    let (tValue, valueI) = generateTVar i in (TypedDeconstructValue tValue s, valueI)
giveUniqTypes2Deconstr i (DeconstructListCons x1 x2) =
    let (tList     , listI    ) = generateTList i
        (tDeconstrs, deconstrI) = giveUniqTypes2Each giveUniqTypes2Deconstr listI (x1 : [x2])
        (tx1 : tx2 : [])        = tDeconstrs
    in  (TypedDeconstructListCons tList tx1 tx2, deconstrI)
giveUniqTypes2Deconstr i DeconstructListNil =
    let (tValue, valueI) = generateTList i in (TypedDeconstructListNil tValue, valueI)
giveUniqTypes2Deconstr i (DeconstructInt n) = (TypedDeconstructInt n, i)
giveUniqTypes2Deconstr i (DeconstructStr s) = (TypedDeconstructStr s, i)
giveUniqTypes2Deconstr i (DeconstructData name xs) =
    let (tExpr     , constrI  ) = generateTConstr i name (length xs)
        (tDeconstrs, deconstrI) = giveUniqTypes2Each giveUniqTypes2Deconstr constrI xs
    in  (TypedDeconstructData tExpr name tDeconstrs, deconstrI)

-- TODO finish typedeclarations
giveUniqTypes2Statement :: Int -> Statement -> (TypedStatement, Int)
giveUniqTypes2Statement i (Decl name decs expr) =
    let (tFunc, funcI    ) = generateTFunc i
        (tDecs, deconstrI) = giveUniqTypes2Each giveUniqTypes2Deconstr funcI decs
        (tExpr, exprI    ) = giveUniqTypes2Expr deconstrI expr
    in  (TypedDecl tFunc name tDecs tExpr, exprI)
giveUniqTypes2Statement i (DataDecl name params arg args) = (TypedDataDecl tExpr name params arg args, i)
    where tExpr = TConstr name (map TParam params)
giveUniqTypes2Statement i (TypeDecl decl) = (TypedTypeDecl (toTypedTypeDecl decl), i)  where
    -- TODO separate pass to make sure that primitives are used correctly
    toTypedTypeDecl :: TypeDeclaration -> TypedTypeDeclaration
    toTypedTypeDecl (FuncTypeDeclaration name t ts  ) = error "t"
    toTypedTypeDecl (DataTypeDeclaration "Int"    []) = TypedDataTypeDeclaration (TPrim PrimInt) "Int" []
    toTypedTypeDecl (DataTypeDeclaration "Bool"   []) = TypedDataTypeDeclaration (TPrim PrimBool) "Bool" []
    toTypedTypeDecl (DataTypeDeclaration "String" []) = TypedDataTypeDeclaration (TPrim PrimStr) "String" []
    toTypedTypeDecl (DataTypeDeclaration "Char"   []) = TypedDataTypeDeclaration (TPrim PrimStr) "Char" []
    toTypedTypeDecl (DataTypeDeclaration name     ts) = TypedDataTypeDeclaration tExpr name typedFields      where
        typedFields = map toTypedTypeDecl ts
        tExpr       = error "t"
