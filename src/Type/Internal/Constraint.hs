module Type.Internal.Constraint
    ( constraints
    , Constraint(..)
    ) where

import           Type.Internal.Util             ( reduce
                                                , generateTVar
                                                , generateTConstr
                                                , generateTFunc
                                                , generateTList
                                                )
import           Type.TypedAST                  ( TExpr(..)
                                                , TPrimitive(..)
                                                , TypedConstructor(..)
                                                , TypedTypeDeclaration(..)
                                                , TypedDeconstructor(..)
                                                , TypedExpr(..)
                                                , TypedProgram(..)
                                                , TypedStatement(..)
                                                )
import qualified Data.Map                       ( Map
                                                , empty
                                                , lookup
                                                , insert
                                                , member
                                                , union
                                                , fromList
                                                )
import           Data.Maybe                     ( isJust )

--
newtype Constraint = Constraint (TExpr, TExpr)
    deriving (Eq, Show)

--
newtype ScopeStack = ScopeStack [Data.Map.Map String TExpr]
    deriving (Eq, Show)

--
getTVarFromExpr :: TypedExpr -> TExpr
getTVarFromExpr (TypedAdd _ _            ) = TPrim PrimInt
getTVarFromExpr (TypedSub _ _            ) = TPrim PrimInt
getTVarFromExpr (TypedMul _ _            ) = TPrim PrimInt
getTVarFromExpr (TypedDiv _ _            ) = TPrim PrimInt
getTVarFromExpr (TypedNegate _           ) = TPrim PrimInt
getTVarFromExpr (TypedAnd _ _            ) = TPrim PrimBool
getTVarFromExpr (TypedOr  _ _            ) = TPrim PrimBool
getTVarFromExpr (TypedEq  _ _            ) = TPrim PrimBool
getTVarFromExpr (TypedNeq _ _            ) = TPrim PrimBool
getTVarFromExpr (TypedGt  _ _            ) = TPrim PrimBool
getTVarFromExpr (TypedMod _ _            ) = TPrim PrimInt
getTVarFromExpr (TypedGe  _ _            ) = TPrim PrimBool
getTVarFromExpr (TypedLt  _ _            ) = TPrim PrimBool
getTVarFromExpr (TypedLe  _ _            ) = TPrim PrimBool
getTVarFromExpr (TypedNot _              ) = TPrim PrimBool
getTVarFromExpr (TypedIfElse tExpr _ _ _ ) = tExpr
getTVarFromExpr (TypedCall   tExpr _ _   ) = tExpr
getTVarFromExpr (TypedLambda tExpr _ _   ) = tExpr
getTVarFromExpr (TypedLetIn  tExpr _ _   ) = tExpr
getTVarFromExpr (TypedMatch tExpr _ _ _  ) = tExpr
getTVarFromExpr (TypedConstr tConstructor) = getTVarFromConstr tConstructor

--
getTVarFromStatement :: TypedStatement -> TExpr
getTVarFromStatement (TypedDecl tExpr _ _ _      ) = tExpr
getTVarFromStatement (TypedDataDecl tExpr _ _ _ _) = tExpr
getTVarFromStatement (TypedTypeDecl typeDecl     ) = getTVarFromTypeDecl typeDecl  where
    getTVarFromTypeDecl (TypedFuncTypeDeclaration tExpr _ _ _) = tExpr
    getTVarFromTypeDecl (TypedDataTypeDeclaration tExpr _ _  ) = tExpr

--
getTVarFromVal :: TypedDeconstructor -> Maybe (String, TExpr)
getTVarFromVal (TypedDeconstructValue tExpr name) = Just (name, tExpr)
getTVarFromVal _ = Nothing

--
getTVarFromDeconstr :: TypedDeconstructor -> TExpr
getTVarFromDeconstr (TypedDeconstructValue tExpr _            ) = tExpr
getTVarFromDeconstr (TypedDeconstructListCons tExpr _ _       ) = tExpr
getTVarFromDeconstr (TypedDeconstructListNil tExpr            ) = tExpr
getTVarFromDeconstr (TypedDeconstructInt     _                ) = TPrim PrimInt
getTVarFromDeconstr (TypedDeconstructStr     _                ) = TPrim PrimStr
getTVarFromDeconstr (TypedDeconstructChar    _                ) = TPrim PrimChar
getTVarFromDeconstr (TypedDeconstructBool    _                ) = TPrim PrimBool
getTVarFromDeconstr (TypedDeconstructData tExpr name deconstrs) = tExpr

--
getTVarFromConstr :: TypedConstructor -> TExpr
getTVarFromConstr (TypedConstructInt  _                  ) = TPrim PrimInt
getTVarFromConstr (TypedConstructChar _                  ) = TPrim PrimChar
getTVarFromConstr (TypedConstructBool _                  ) = TPrim PrimBool
getTVarFromConstr (TypedConstructStr  _                  ) = TPrim PrimStr
getTVarFromConstr (TypedConstructValue tExpr _           ) = tExpr
getTVarFromConstr (TypedConstructListNil tExpr           ) = tExpr
getTVarFromConstr (TypedConstructListCons tExpr _ _      ) = tExpr
getTVarFromConstr (TypedConstructExpr typedExpr          ) = getTVarFromExpr typedExpr
getTVarFromConstr (TypedConstructData tExpr name tConstrs) = tExpr

getTVarFromTTDecl :: TypedTypeDeclaration -> TExpr
getTVarFromTTDecl (TypedFuncTypeDeclaration tExpr _ _ _) = tExpr
getTVarFromTTDecl (TypedDataTypeDeclaration tExpr _ _  ) = tExpr

-- recursively look up the scope stacks to see if a var exists
getType :: String -> ScopeStack -> Maybe TExpr
getType key (ScopeStack []      ) = Nothing
getType key (ScopeStack (x : xs)) = case Data.Map.lookup key x of
    Just val -> Just val  -- found? just return
    Nothing  -> getType key (ScopeStack xs)  -- search in parent stacks

-- TODO think about the scope stack logic
-- TODO should there be a separate pass in the beginning where the initial scope stack should be created?
constraints :: TypedProgram -> Int -> ([Constraint], Int)
constraints (TypedProg stmts expr) i =
    -- accumulate the constraints from each stmt in the program
    let (stmtConstraints, stmtScopeStack, stmtsI) = reduce f initAcc stmts              where
                -- empty constraints and an empty scope stack
                initAcc = (initialConstraints, initialScopeStack, i)                  where
                    initialConstraints = []
                    initialScopeStack  = ScopeStack [Data.Map.empty]
                -- reducer; take a single statement, and merge its constraints and scope with the acc
                f = \stmt (cs, ScopeStack (s : ss), index) ->
                    let (stmtCs, newScopeStack, stmtI) = constraintsInStatement (ScopeStack (s : ss)) stmt index
                        newConstraints                 = stmtCs ++ cs
                    in  (newConstraints, newScopeStack, stmtI)
        -- from expr
        (exprConstraints, _, _) = constraintsInExpr stmtScopeStack expr stmtsI
        -- union the constraints in the statements and the expr
    in  (stmtConstraints ++ exprConstraints, stmtsI)

--
foldToTFunc :: [TExpr] -> TExpr
foldToTFunc []             = error "unreachable"
foldToTFunc (x       : []) = x
foldToTFunc (x1 : x2 : []) = TFunc x1 x2
foldToTFunc (x       : xs) = TFunc x (foldToTFunc xs)

----------------------------------------------------------------
----------------------------------------------------------------
----------------------------------------------------------------
----------------------------------------------------------------
----------------------------------------------------------------
----------------------------------------------------------------
----------------------------------------------------------------
----------------------------------------------------------------

type Acc = ([Constraint], Int)
type ConstraintExtractor a = ScopeStack -> a -> Int -> ([Constraint], ScopeStack, Int)
accConstraints :: ConstraintExtractor a -> Acc -> ScopeStack -> [a] -> Acc
accConstraints f initAcc (ScopeStack (s : ss)) args = reduce fWrapped initAcc args  where
    fWrapped = \x (accCs, accI) -> case f (ScopeStack $ Data.Map.empty : s : ss) x accI of
        (cs, _, accINew) -> (cs ++ accCs, accINew)

--
constraintsInStatement :: ScopeStack -> TypedStatement -> Int -> ([Constraint], ScopeStack, Int)
--
constraintsInStatement (ScopeStack (s : ss)) (TypedDecl tExpr name deconstrs typedExpr) i =
    let -- some new random var to link tExpr to the type of the declaration
        -- linking via a temporary type var because comparing recursive structure is more expensive
        (tmpTVar, iNew) = generateTVar i
        -- stack with this decl added
        s'              = Data.Map.insert name tmpTVar s
        --
        stmtConstraints = [constraint1, constraint2]              where
                -- tmpTVar = tExpr
                constraint1 = Constraint (tmpTVar, tExpr)
                -- tmpTVar = inferredType
                constraint2 = Constraint (tmpTVar, inferredType)                  where
                    --
                    inferredType = foldToTFunc $ argTypes ++ [finalRetType]                      where
                        argTypes     = map getTVarFromDeconstr deconstrs
                        finalRetType = getTVarFromExpr typedExpr
        -- pass a new stack on top, so that they are no persistent
        (exprConstraints, _, iFinal) = constraintsInExpr (ScopeStack $ Data.Map.empty : s' : ss) typedExpr iNew
        --
    in  (exprConstraints ++ stmtConstraints, ScopeStack (s' : ss), iFinal)
    --
constraintsInStatement (ScopeStack (s : ss)) (TypedDataDecl tExpr name targs _ _) i =
    let --
        (tmpTVar, iNew) = generateTVar i
        --
        s'              = Data.Map.insert name tmpTVar s
        --
        constraintList  = [constraint1, constraint2]              where
                -- tmpTVar = tExpr
                constraint1 = Constraint (tmpTVar, tExpr)
                -- tmpTVar = inferredType
                constraint2 = Constraint (tmpTVar, inferredType) where inferredType = TConstr name (map TParam targs)
    in  (constraintList, ScopeStack (s' : ss), iNew)
--
constraintsInStatement (ScopeStack (s : ss)) (TypedTypeDecl typeDecl) i = error "t"  where
    constraintsInStatement' :: ScopeStack -> TypedTypeDeclaration -> Int -> ([Constraint], ScopeStack, Int)
    constraintsInStatement' (ScopeStack (s : ss)) (TypedFuncTypeDeclaration tExpr name ttDecl ttDecls) i =
        let --
            (tmpTVar, iNew) = generateTVar i
            --
            s'              = Data.Map.insert name tmpTVar s
            constraintList  = [constraint1, constraint2]                  where
                -- tmpTVar = tExpr
                    constraint1 = Constraint (tmpTVar, tExpr)
                    constraint2 = Constraint (tmpTVar, inferredType)
                        where inferredType = foldToTFunc $ map getTVarFromTTDecl (ttDecl : ttDecls)
            --
        in  (constraintList, ScopeStack (s' : ss), iNew)
    constraintsInStatement' (ScopeStack (s : ss)) (TypedDataTypeDeclaration tExpr name ttDecls) i =
        let --
            (tmpTVar, iNew) = generateTVar i
            --
            s'              = Data.Map.insert name tmpTVar s
            constraintList  = [constraint1, constraint2]                  where
                -- tmpTVar = tExpr
                    constraint1 = Constraint (tmpTVar, tExpr)
                    constraint2 = Constraint (tmpTVar, inferredType)
                        where inferredType = TConstr name (map getTVarFromTTDecl ttDecls)
            --
        in  (constraintList, ScopeStack (s' : ss), iNew)

--
constraintsInConstr :: ScopeStack -> TypedConstructor -> Int -> ([Constraint], ScopeStack, Int)
constraintsInConstr sstack (TypedConstructInt  _) i = ([], sstack, i)
constraintsInConstr sstack (TypedConstructBool _) i = ([], sstack, i)
constraintsInConstr sstack (TypedConstructStr  _) i = ([], sstack, i)
constraintsInConstr (ScopeStack (s : ss)) (TypedConstructValue tExpr name) i =
    let --
        (tmpTVar, iNew) = generateTVar i
        --
    in  case getType name (ScopeStack $ s : ss) of
            Just exprType -> ([Constraint (exprType, tExpr)], ScopeStack $ s : ss, iNew)
            Nothing       -> error $ "not found in stack: "
constraintsInConstr sstack (TypedConstructListNil tExpr) i =
    let --
        (tmpTVar    , iNew  ) = generateTVar i
        (tmpElemTVar, iFinal) = generateTVar iNew
        -- tmpTVar = [a]
        constraintList        = [constraint] where constraint = Constraint (tmpTVar, TList tmpElemTVar)
        --
    in  (constraintList, sstack, iFinal)
-- generate tvartmp
-- for each elem tvar t, t = tvartmp
-- texpr = list tvartmp
constraintsInConstr (ScopeStack (s : ss)) (TypedConstructListCons tExpr x xs) i =
    let --
        (tmpListElemTVar, iNew) = generateTVar i
        --
        constraintList          = [listConstraint, xConstraint, xsConstraint]              where
                --
                xConstraint    = Constraint (tmpListElemTVar, getTVarFromConstr x)

                --
                xsConstraint   = Constraint (getTVarFromConstr xs, TList tmpListElemTVar)

                -- tmpTVar = TList newvar
                listConstraint = Constraint (tExpr, TList tmpListElemTVar)
        --
        (elementCs, iFinal) = accConstraints constraintsInConstr ([], iNew) (ScopeStack (s : ss)) (x : [xs])
        --
    in  (elementCs ++ constraintList, ScopeStack (s : ss), iFinal)
constraintsInConstr (ScopeStack (s : ss)) (TypedConstructExpr typedExpr) i =
    let --
        (tmpTVar, iNew)              = generateTVar i
        -- pass a new stack on top, so that they are no persistent
        (exprConstraints, _, iFinal) = constraintsInExpr (ScopeStack $ Data.Map.empty : s : ss) typedExpr iNew
        --
    in  (exprConstraints, ScopeStack (s : ss), iFinal)
--
constraintsInConstr (ScopeStack (s : ss)) (TypedConstructData tExpr name args) i =
    let --
        (tmpTVar    , iNew  ) = generateTVar i
        (constraints, iFinal) = (basicConstraint : structuredConstraint : recursiveCs, iFinal)              where
                basicConstraint       = Constraint (tmpTVar, tExpr)
                structuredConstraint  = Constraint (tmpTVar, TConstr name (map getTVarFromConstr args))
                   --  TypedConstructData TExpr String [TypedConstructor]
                (recursiveCs, iFinal) = reduce f initAcc args                  where
                    f = \x (accConstraints, accI) -> case constraintsInConstr (ScopeStack $ Data.Map.empty : s : ss) x accI of
                        (cs, _, accINew) -> (cs ++ accConstraints, accINew)
                    initAcc = ([], iNew)
    in  (constraints, ScopeStack (s : ss), iFinal)

--
constraintsInDeconstr :: ScopeStack -> TypedDeconstructor -> Int -> ([Constraint], ScopeStack, Int)
constraintsInDeconstr sStack (TypedDeconstructValue tExpr name) i = case getType name sStack of
    Just exprType -> ([Constraint (exprType, tExpr)], sStack, i)
    Nothing       -> error $ "not found in stack: " ++ (show name) ++ ", " ++ (show sStack)
constraintsInDeconstr (ScopeStack (s : ss)) (TypedDeconstructListCons tExpr x0 x1) i =
    let --
        (tmpTVar        , iNew   ) = generateTVar i
        (tmpListElemTVar, iNewNew) = generateTVar iNew
        --
        constraintList             = listConstraint : elementConstraints              where
                -- forall t, tmpTVar = t
                elementConstraints = map (\elem -> Constraint (tmpTVar, getTVarFromDeconstr elem)) (x0 : [x1])
                -- tmpTVar = TList newvar
                listConstraint     = Constraint (tmpTVar, TList tmpListElemTVar)
        --
        (elementCs, iFinal) = accConstraints constraintsInDeconstr ([], iNewNew) (ScopeStack (s : ss)) (x0 : [x1])
        --
    in  (elementCs ++ constraintList, ScopeStack (s : ss), iFinal)
constraintsInDeconstr sStack (TypedDeconstructListNil tExpr) i =
    let --
        (tmpTVar, iNew) = generateTVar i
    in  ([Constraint (tExpr, TList tmpTVar)], sStack, iNew)
constraintsInDeconstr sStack (TypedDeconstructBool value            ) i = ([], sStack, i)
constraintsInDeconstr sStack (TypedDeconstructInt  value            ) i = ([], sStack, i)
constraintsInDeconstr sStack (TypedDeconstructStr  value            ) i = ([], sStack, i)
constraintsInDeconstr sStack (TypedDeconstructData tExpr name fields) i = error ""

constraintsInBinaryExpr :: TExpr -> ScopeStack -> TypedExpr -> TypedExpr -> Int -> ([Constraint], ScopeStack, Int)
constraintsInBinaryExpr tExpr sStack lhs rhs i =
    let  --
        (lhsTVar, lhsCs, _, iLHS) = constraintsInExpr' (Just $ TPrim PrimInt) sStack lhs i
        (rhsTVar, rhsCs, _, iRHS) = constraintsInExpr' (Just $ TPrim PrimInt) sStack rhs iLHS
    in  (Constraint (lhsTVar, rhsTVar) : lhsCs ++ rhsCs, sStack, iRHS)


-- extract constraints, bind and return the new representative tvar
--
-- # Returns
--
-- (tExpr, cs, ss, i) where
--     tExpr = new temporary type variable
--     cs = constraints extracted from `expr` as well as `tExpr` bound to the first optional arg, if provided
constraintsInExpr' :: Maybe TExpr -> ScopeStack -> TypedExpr -> Int -> (TExpr, [Constraint], ScopeStack, Int)
constraintsInExpr' tExprOpt (ScopeStack (s : ss)) expr i =
    let --
        (tmpTVar, iNew)              = generateTVar i
        -- expr :: tmpTVar
        constraintOnExpr             = Constraint (tmpTVar, getTVarFromExpr expr)
        -- extract constraints from `expr`
        (exprConstraints, _, iFinal) = constraintsInExpr (ScopeStack $ Data.Map.empty : s : ss) expr iNew
        --
        allConstraints               = case tExprOpt of
            Just tExpr -> Constraint (tmpTVar, tExpr) : constraintOnExpr : exprConstraints
            Nothing    -> constraintOnExpr : exprConstraints
    in  (tmpTVar, allConstraints, ScopeStack (s : ss), iFinal)

--
constraintsInExpr :: ScopeStack -> TypedExpr -> Int -> ([Constraint], ScopeStack, Int)
constraintsInExpr sStack (TypedAdd lhs rhs) i = constraintsInBinaryExpr (TPrim PrimInt) sStack lhs rhs i
constraintsInExpr sStack (TypedSub lhs rhs) i = constraintsInBinaryExpr (TPrim PrimInt) sStack lhs rhs i
constraintsInExpr sStack (TypedMul lhs rhs) i = constraintsInBinaryExpr (TPrim PrimInt) sStack lhs rhs i
constraintsInExpr sStack (TypedDiv lhs rhs) i = constraintsInBinaryExpr (TPrim PrimInt) sStack lhs rhs i
constraintsInExpr sStack (TypedEq  lhs rhs) i = constraintsInBinaryExpr (TPrim PrimInt) sStack lhs rhs i
constraintsInExpr sStack (TypedNeq lhs rhs) i = constraintsInBinaryExpr (TPrim PrimInt) sStack lhs rhs i
constraintsInExpr sStack (TypedGt  lhs rhs) i = constraintsInBinaryExpr (TPrim PrimInt) sStack lhs rhs i
constraintsInExpr sStack (TypedMod lhs rhs) i = constraintsInBinaryExpr (TPrim PrimInt) sStack lhs rhs i
constraintsInExpr sStack (TypedGe  lhs rhs) i = constraintsInBinaryExpr (TPrim PrimInt) sStack lhs rhs i
constraintsInExpr sStack (TypedLt  lhs rhs) i = constraintsInBinaryExpr (TPrim PrimInt) sStack lhs rhs i
constraintsInExpr sStack (TypedLe  lhs rhs) i = constraintsInBinaryExpr (TPrim PrimInt) sStack lhs rhs i
constraintsInExpr sStack (TypedAnd lhs rhs) i = constraintsInBinaryExpr (TPrim PrimBool) sStack lhs rhs i
constraintsInExpr sStack (TypedOr  lhs rhs) i = constraintsInBinaryExpr (TPrim PrimBool) sStack lhs rhs i
constraintsInExpr sStack (TypedNegate expr) i = case constraintsInExpr' (Just $ TPrim PrimInt) sStack expr i of
    (_, cs, newStack, iNew) -> (cs, newStack, iNew)
constraintsInExpr sStack (TypedNot expr) i = case constraintsInExpr' (Just $ TPrim PrimBool) sStack expr i of
    (_, cs, newStack, iNew) -> (cs, newStack, iNew)
constraintsInExpr sStack (TypedIfElse tIfElse cond ifTrue ifFalse) i =
    let --
        (_       , condCs   , _, iCond   ) = constraintsInExpr' (Just $ TPrim PrimBool) sStack cond i
        (tIfTrue , ifTrueCs , _, iIfTrue ) = constraintsInExpr' Nothing sStack ifTrue iCond
        (tIfFalse, ifFalseCs, _, iIfFalse) = constraintsInExpr' Nothing sStack ifFalse iIfTrue
    in  ((Constraint (tIfTrue, tIfFalse)) : condCs ++ ifTrueCs ++ ifFalseCs, sStack, iIfFalse)
constraintsInExpr sStack (TypedCall tCall func arg) i =
    let --
        (retTVar, iNew) = generateTVar i

        -- tCall = retTVar
        callConstraint  = Constraint (retTVar, tCall)

        -- func :: args -> retTVar
        funcConstraint  = Constraint (tExprFromStructure, tExprFromFunc)              where
                argType            = getTVarFromExpr arg
                tExprFromFunc      = getTVarFromExpr func
                tExprFromStructure = TFunc argType retTVar

        (csArg, _, iFinal) = constraintsInExpr sStack arg iNew
    in  (funcConstraint : callConstraint : csArg, sStack, iFinal)
-- | TypedLambda TExpr TypedDeconstructor [TypedDeconstructor] TypedExpr
-- TODO need to add a new scope
constraintsInExpr (ScopeStack (s : ss)) (TypedLambda tLambda arg body) i =
    let --
        argType            = getTVarFromDeconstr arg
        bodyType           = getTVarFromExpr body
        tExprFromStructure = TFunc argType bodyType

        --
        newScope =
            Data.Map.fromList
                $ [ case getTVarFromVal arg of
                        Just x  -> x
                        Nothing -> error "unreachable"
                  ]
        --
        (constraints, iArgs) = (basicC : csFromBody, iArgs)              where
                basicC                    = Constraint (tLambda, tExprFromStructure)
                (_, csFromBody, _, iBody) = constraintsInExpr' (Just bodyType) (ScopeStack $ newScope : s : ss) body i
    --
    in  (constraints, ScopeStack $ s : ss, iArgs)
constraintsInExpr (ScopeStack (s : ss)) (TypedLetIn tLetIn stmts expr    ) i = error "t"
constraintsInExpr sStack                (TypedConstr c                   ) i = constraintsInConstr sStack c i
constraintsInExpr (ScopeStack (s : ss)) (TypedMatch tMatch expr arm1 arms) i = error "t"

{-
unify :: [TExpr] -> Map Expr TExpr
unify []       = Data.Map.empty
unify (x : xs) = Data.Map.empty
-}
