-- ADAPTED FROM https://home.cs.colorado.edu/~bec/courses/csci5535-s09/slides/ML_Type_Inference_and_Unification.pdf
module Type.Internal.ConstraintSolver
    ( solveConstraints
    , Substitution(..)
    , applySubstitution
    ) where

import           Type.TypedAST                  ( TExpr(..)
                                                , TPrimitive(..)
                                                , TypedConstructor(..)
                                                , TypedDeconstructor(..)
                                                , TypedExpr(..)
                                                , TypedProgram(..)
                                                , TypedStatement(..)
                                                )
import           Type.Internal.Constraint       ( constraints
                                                , Constraint(..)
                                                )
import           Data.Map                       ( Map
                                                , empty
                                                )

--
newtype Substitution = Substitution (TExpr, TExpr)
    deriving (Eq, Show)

-- # Returns
--
-- a set of substitutinos that can be applied directly to the AST
solveConstraints :: [Constraint] -> [Substitution]
solveConstraints cs = solveConstraints' cs []  where
    -- basically the algorithm in the link at the top
    solveConstraints' :: [Constraint] -> [Substitution] -> [Substitution]
    solveConstraints' [] ss = ss
    solveConstraints' (Constraint (lhs, rhs) : cs) ss =
     -- for each c in cs
     --     subs = unify c
     --     for s in subs
     --         apply s for the rest of cs
     --         apply s for all ss
     --         store the s in ss
     --         recurse
        let -- substitutions to unify `lhs` and `rhs`
            subs     = unify lhs rhs
            csSubbed = map (applySubsInConstraint subs) cs
            ssSubbed = map (applySubsInSubstitution subs) ss
        in  solveConstraints' csSubbed (subs ++ ssSubbed)

applySubsInConstraint :: [Substitution] -> Constraint -> Constraint
applySubsInConstraint []       c = c
applySubsInConstraint (s : ss) c = applySubsInConstraint ss (applySubInConstraint s c)

applySubsInSubstitution :: [Substitution] -> Substitution -> Substitution
applySubsInSubstitution []       sub = sub
applySubsInSubstitution (s : ss) sub = applySubsInSubstitution ss (applySubInSubstitution s sub)

applySubInConstraint :: Substitution -> Constraint -> Constraint
applySubInConstraint s (Constraint (lhs, rhs)) = Constraint (lhsSubbed, rhsSubbed)  where
    lhsSubbed = applySubstitution s lhs
    rhsSubbed = applySubstitution s rhs

applySubInSubstitution :: Substitution -> Substitution -> Substitution
applySubInSubstitution s (Substitution (lhs, rhs)) = Substitution (lhs, rhsSubbed)
    where
    --    lhsSubbed = applySubstitution s lhs
          rhsSubbed = applySubstitution s rhs

-- # Parameters
--
-- 1.
-- 2.
--
-- # Returns
--
-- a set of substitutions needed to unify `$1` and `$2`
-- TODO eventually have this return `Either`, so that we can do proper error messaging
unify :: TExpr -> TExpr -> [Substitution]
unify (TConstr nameL fieldsL) (TConstr nameR fieldsR) = if nameL == nameR && length fieldsL == length fieldsR
    then concat $ map (\(x, y) -> unify x y) $ zip fieldsL fieldsR
    else error "type constr mismatch"
unify (TFunc argL bodyL) (TFunc argR bodyR) = (unify argL argR) ++ (unify bodyL bodyR)
unify (TList  typeL    ) (TList  typeR    ) = unify typeL typeR
unify (TParam nameL    ) (TParam nameR    ) = [Substitution (TParam nameL, TParam nameR)]
unify (TPrim  primL    ) (TPrim  primR    ) = case (primL, primR) of
    (PrimInt , PrimInt ) -> []
    (PrimBool, PrimBool) -> []
    (PrimStr , PrimStr ) -> []
    -- TODO add string = list char
    (PrimChar, PrimChar) -> []
    _                    -> error "type mismatch"
unify (TVar name) anything    = [Substitution (TVar name, anything)]
unify anything    (TVar name) = [Substitution (TVar name, anything)]
unify _           _           = error "type mismatch"

--
applySubstitution :: Substitution -> TExpr -> TExpr
-- write it with `case` like this so that we can refer to `tExpr` without
-- having to type out the whole pattern
applySubstitution (Substitution (from, to)) tExpr = case tExpr of
    (TVar _) -> if from == tExpr then to else tExpr
    (TConstr name fields) ->
        let newTExpr = TConstr name subbedFields                  where
            subbedFields = map (applySubstitution $ Substitution (from, to)) fields
        in  if from == newTExpr then to else newTExpr
    (TFunc arg body) ->
        let newTExpr = TFunc subbedArg subbedBody                  where
            subbedArg  = applySubstitution (Substitution (from, to)) arg
            subbedBody = applySubstitution (Substitution (from, to)) body
        in  if from == newTExpr then to else newTExpr
    (TList member) ->
        let newTExpr = TList subbedMember                  where
            subbedMember = applySubstitution (Substitution (from, to)) member
        in  if from == newTExpr then to else newTExpr
    (TParam _) -> if from == tExpr then to else tExpr
    (TPrim  _) -> if from == tExpr then to else tExpr
