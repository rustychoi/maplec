module Type.Internal.Util
    ( reduce
    , generateTVar
    , generateTConstr
    , generateTFunc
    , generateTList
    ) where

import           Text.Printf                    ( printf )
import           Type.TypedAST                  ( TExpr(..)
                                                , TypedConstructor(..)
                                                , TypedDeconstructor(..)
                                                , TypedTypeDeclaration(..)
                                                , TypedExpr(..)
                                                , TypedProgram(..)
                                                , TPrimitive(..)
                                                , TypedStatement(..)
                                                )

-- like `fold`, but this allows the acc value to be of an arbitary type
--
-- `f` is guaranteed to be applied to elements in `xs` from left to right.
--
-- # Parameters
--
-- 1. `f :: a -> b -> b`: mapping from `(x, acc)` to a new value for `acc`, where
--    `x` is an element in `xs`
-- 2. `acc :: b`: the accumulated value to keep track of
-- 3. `xs :: [a]`: elements to reduce
--
-- # Returns
--
-- TODO move this to a general utils file
reduce :: (a -> b -> b) -> b -> [a] -> b
reduce f acc []       = acc
reduce f acc (x : xs) = reduce f (f x acc) xs

--
generateTVar :: Int -> (TExpr, Int)
generateTVar i = (newTVar, i + 1) where newTVar = TVar (".t" ++ printf "%04d" i)

--
generateTConstr :: Int -> String -> Int -> (TExpr, Int)
generateTConstr i name num =
    let (typedConstrs, ti) = reduce f initAcc is              where
                f       = \_ (tcs, accI) -> let (tc, newI) = generateTVar accI in (tc : tcs, newI)
                initAcc = ([], i)
                is      = [0 .. num]
    in  (TConstr name typedConstrs, ti)

--
generateTFunc :: Int -> (TExpr, Int)
generateTFunc i =
    let (targ, argI) = generateTVar i
        (tret, retI) = generateTVar argI
    in  (TFunc targ tret, retI)
--
generateTList :: Int -> (TExpr, Int)
generateTList i = let (tvar, ti) = generateTVar i in (TList tvar, ti)
