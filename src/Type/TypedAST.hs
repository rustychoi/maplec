module Type.TypedAST
    ( TExpr(..)
    , TPrimitive(..)
    , TypedConstructor(..)
    , TypedTypeDeclaration(..)
    , TypedDeconstructor(..)
    , TypedExpr(..)
    , TypedProgram(..)
    , TypedStatement(..)
    ) where

import           AST                            ( Expr(..)
                                                , Statement(..)
                                                , Constructor(..)
                                                , Deconstructor(..)
                                                )

--
data TypedProgram = TypedProg [TypedStatement] TypedExpr
    deriving (Eq, Show)
--
data TypedTypeDeclaration = TypedFuncTypeDeclaration TExpr String TypedTypeDeclaration [TypedTypeDeclaration]
                          | TypedDataTypeDeclaration TExpr String [TypedTypeDeclaration]
    deriving (Eq, Show)
--
data TypedStatement = TypedDecl TExpr String [TypedDeconstructor] TypedExpr
                    | TypedTypeDecl TypedTypeDeclaration
                    | TypedDataDecl TExpr String [String] (String, [String]) [(String, [String])]
    deriving (Eq, Show)
--
data TypedConstructor = TypedConstructBool Bool
                      | TypedConstructChar Char
                      | TypedConstructData TExpr String [TypedConstructor]
                      | TypedConstructExpr TypedExpr
                      | TypedConstructInt Int
                      | TypedConstructListCons TExpr TypedConstructor TypedConstructor
                      | TypedConstructListNil TExpr
                      | TypedConstructStr String
                      | TypedConstructValue TExpr String
    deriving (Eq, Show)
--
data TypedDeconstructor = TypedDeconstructBool Bool
                        | TypedDeconstructChar Char
                        | TypedDeconstructData TExpr String [TypedDeconstructor]
                        | TypedDeconstructInt Int
                        | TypedDeconstructListCons TExpr TypedDeconstructor TypedDeconstructor
                        | TypedDeconstructListNil TExpr
                        | TypedDeconstructStr String
                        | TypedDeconstructValue TExpr String
    deriving (Eq, Show)
--
data TypedExpr = TypedAdd TypedExpr TypedExpr
               | TypedSub TypedExpr TypedExpr
               | TypedMul TypedExpr TypedExpr
               | TypedDiv TypedExpr TypedExpr
               | TypedNegate TypedExpr
               | TypedAnd TypedExpr TypedExpr
               | TypedOr TypedExpr TypedExpr
               | TypedEq TypedExpr TypedExpr
               | TypedNeq TypedExpr TypedExpr
               | TypedGt TypedExpr TypedExpr
               | TypedMod TypedExpr TypedExpr
               | TypedGe TypedExpr TypedExpr
               | TypedLt TypedExpr TypedExpr
               | TypedLe TypedExpr TypedExpr
               | TypedNot TypedExpr
               | TypedIfElse TExpr TypedExpr TypedExpr TypedExpr
               | TypedCall TExpr TypedExpr TypedExpr
               | TypedLambda TExpr TypedDeconstructor TypedExpr
               | TypedLetIn TExpr [TypedStatement] TypedExpr
               | TypedConstr TypedConstructor
               | TypedMatch TExpr TypedExpr (TypedDeconstructor, TypedExpr) [(TypedDeconstructor, TypedExpr)]
    deriving (Eq, Show)

--
data TExpr = TConstr String [TExpr]
           | TFunc TExpr TExpr
           | TList TExpr
           | TParam String
           | TPrim TPrimitive
           | TVar String
    deriving (Eq, Show)
--
data TPrimitive = PrimBool
                | PrimChar
                | PrimInt
                | PrimStr
    deriving (Eq, Show)
