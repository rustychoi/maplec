module Optimization.AST.Util
    ( propagateExprOptim2Statement
    , propagateExprOptim2Expr
    , propagateExprOptim2Constr
    , makeLegible
    ) where

import           AST                            ( Expr(..)
                                                , Statement(..)
                                                , Constructor(..)
                                                )

-- TODO move this back to constant folding dir, because pulling out additive/multiplicative constants
-- would be useful in this func
--
-- Not really an optimization, but use some heuristics to make the AST more `legible`.
--
-- For example, do the following:
-- 1. in a sum, make sure that the constant appear last; `x + 3 + y` should be `x + y + 3`
-- 1. in a sum, make sure that the variables are sorted alphabetically; `y + x + 3` should be `x + y + 3`
-- 1. in a sum, use the infix operators to represent sign rather than using negation; `x + -3` should be `x - 3`
makeLegible :: Expr -> Expr
--
makeLegible (Add lhs          (Negate rhs)) = Sub (makeLegible lhs) (makeLegible rhs)
makeLegible (Add (Negate lhs) rhs         ) = Sub (makeLegible rhs) (makeLegible lhs)
makeLegible (Add lhs (Constr (ConstructInt n))) =
    if n < 0 then Sub (makeLegible lhs) (Constr $ ConstructInt (-n)) else Add (makeLegible lhs) (Constr $ ConstructInt n)
makeLegible (Add (Constr (ConstructInt n)) rhs) =
    if n < 0 then Sub (makeLegible rhs) (Constr $ ConstructInt (-n)) else Add (Constr $ ConstructInt n) (makeLegible rhs)
--
makeLegible (Sub lhs (Negate rhs)) = Add (makeLegible lhs) (makeLegible rhs)
makeLegible (Sub lhs (Constr (ConstructInt n))) =
    if n < 0 then Add (makeLegible lhs) (Constr $ ConstructInt (-n)) else Sub (makeLegible lhs) (Constr $ ConstructInt n)
--
makeLegible (Mul expr (Constr (ConstructInt n))) = Mul (Constr $ ConstructInt n) (makeLegible expr)
-- rest are just propagations
makeLegible expr = propagateExprOptim2Expr makeLegible expr

-- Imagine that an optimization, say `f`, only mainly concerns to a subset of the variants, like `Add` and `Sub`.
--
-- Instead of defining a branch for every variant of the data type, we want to be able to write `f` like so:
-- ```haskell
-- f (Add lhs rhs) = ... do actual optimization
-- f (Sub lhs rhs) = ... do actual optimization
-- f rest = propagateExprOptim2Expr f rest
-- ```
--
-- This helps propagate `f` recursively throughout variants that are not `Add` or `Sub`, but may contain it.
--
-- # Parameters
--
-- 1. `f`: the optimization function to propagate
-- 2. `expr`: expr throughout which `f` will be propagated
propagateExprOptim2Expr :: (Expr -> Expr) -> Expr -> Expr
propagateExprOptim2Expr f (Add lhs rhs               ) = Add (f lhs) (f rhs)
propagateExprOptim2Expr f (Sub lhs rhs               ) = Sub (f lhs) (f rhs)
propagateExprOptim2Expr f (Mul lhs rhs               ) = Mul (f lhs) (f rhs)
propagateExprOptim2Expr f (Div lhs rhs               ) = Div (f lhs) (f rhs)
propagateExprOptim2Expr f (And lhs rhs               ) = And (f lhs) (f rhs)
propagateExprOptim2Expr f (Or  lhs rhs               ) = Or (f lhs) (f rhs)
propagateExprOptim2Expr f (Eq  lhs rhs               ) = Eq (f lhs) (f rhs)
propagateExprOptim2Expr f (Neq lhs rhs               ) = Neq (f lhs) (f rhs)
propagateExprOptim2Expr f (Gt  lhs rhs               ) = Gt (f lhs) (f rhs)
propagateExprOptim2Expr f (Mod lhs rhs               ) = Mod (f lhs) (f rhs)
propagateExprOptim2Expr f (Ge  lhs rhs               ) = Ge (f lhs) (f rhs)
propagateExprOptim2Expr f (Lt  lhs rhs               ) = Lt (f lhs) (f rhs)
propagateExprOptim2Expr f (Le  lhs rhs               ) = Le (f lhs) (f rhs)
propagateExprOptim2Expr f (Negate expr               ) = Negate $ f expr
propagateExprOptim2Expr f (Not    expr               ) = Not $ f expr
propagateExprOptim2Expr f (IfElse cond ifTrue ifFalse) = IfElse (f cond) (f ifTrue) (ifFalse)
propagateExprOptim2Expr f (Call   func  arg          ) = Call (f func) (f arg)
propagateExprOptim2Expr f (Lambda arg   body         ) = Lambda arg (f body)
propagateExprOptim2Expr f (LetIn  stmts expr         ) = LetIn (map (propagateExprOptim2Statement f) stmts) (f expr)
propagateExprOptim2Expr f (Constr c                  ) = Constr $ (propagateExprOptim2Constr f) c
propagateExprOptim2Expr f (Match expr arm1 arms) =
    let applyToArm = \x -> case x of
            (d, armBody) -> (d, f armBody)
    in  Match (f expr) (applyToArm arm1) (map applyToArm arms)

-- propagate `Expr` optimization throughout variants of `Statement`
propagateExprOptim2Statement :: (Expr -> Expr) -> Statement -> Statement
propagateExprOptim2Statement f (Decl name args expr) = Decl name args (f expr)
propagateExprOptim2Statement _ s                     = s

-- propagate `Expr` optimization throughout variants of `Constructor`
propagateExprOptim2Constr :: (Expr -> Expr) -> Constructor -> Constructor
propagateExprOptim2Constr f (ConstructListCons head tail) =
    let headFolded = propagateExprOptim2Constr f head
        tailFolded = propagateExprOptim2Constr f tail
    in  ConstructListCons headFolded tailFolded
propagateExprOptim2Constr f (ConstructData name args) = ConstructData name $ map (propagateExprOptim2Constr f) args
propagateExprOptim2Constr f (ConstructExpr expr     ) = ConstructExpr $ f expr
propagateExprOptim2Constr _ c                         = c
