module Optimization.AST.Algebraic.ConstantFold
    ( constantFoldExpr
    ) where

import           AST                            ( Expr(..)
                                                , Constructor(..)
                                                , Deconstructor(..)
                                                )
import           Data.List
import           Optimization.AST.Util          ( propagateExprOptim2Expr
                                                , propagateExprOptim2Constr
                                                )
import           Optimization.AST.Algebraic.FoldAdditiveConstants
                                                ( foldAdditiveConsts )
import           Optimization.AST.Algebraic.FoldMultiplicativeConstants
                                                ( foldMultiplicativeConsts )

-- `fold` constant expressions by doing the following:
-- 1. in a sum, accumulate all constants into a single constant
-- 1. in a product, accumulate all constants into a single constant
constantFoldExpr :: Expr -> Expr
constantFoldExpr = foldMultiplicativeConsts . foldAdditiveConsts . foldNegationExpr

-- `fold` negations by doing the following:
-- 1. `negate int` is folded to `-int`
-- 1. `negate negate expr` is folded to `expr`
foldNegationExpr :: Expr -> Expr
-- actual optimizations
foldNegationExpr (Negate (Constr (ConstructInt n))) = Constr $ ConstructInt (-n)
foldNegationExpr (Negate (Negate expr            )) = foldNegationExpr expr
-- rest are just propagations
foldNegationExpr expr                               = propagateExprOptim2Expr foldNegationExpr expr
