module Optimization.AST.Algebraic.FoldAdditiveConstants
    ( foldAdditiveConsts
    ) where

import           AST                            ( Expr(..)
                                                , Constructor(..)
                                                )
import           Optimization.AST.Util          ( propagateExprOptim2Expr )
--
int :: Int -> Expr
int = Constr . ConstructInt

--
foldAdditiveConsts :: Expr -> Expr
foldAdditiveConsts (Negate expr                ) = folder $ Negate expr
foldAdditiveConsts (Add lhs rhs                ) = folder $ Add lhs rhs
foldAdditiveConsts (Sub lhs rhs                ) = folder $ Sub lhs rhs
foldAdditiveConsts (Constr (ConstructInt  i   )) = folder $ int i
foldAdditiveConsts (Constr (ConstructExpr expr)) = folder $ Constr $ ConstructExpr expr
foldAdditiveConsts expr                          = propagateExprOptim2Expr foldAdditiveConsts expr

--
folder :: Expr -> Expr
folder = accumulateAdditiveConsts . pullAdditiveConstFromExpr

-- helper for `foldConstExpr`
--
-- consumes the output of `pullAdditiveConstFromExpr`
accumulateAdditiveConsts :: (Expr, Int) -> Expr
-- actual optimizations
accumulateAdditiveConsts (Constr (ConstructInt n), i) = int (n + i)
accumulateAdditiveConsts (Add lhs rhs            , i) = Add (Add lhs rhs) (int i)
accumulateAdditiveConsts (Sub lhs rhs            , i) = Sub (Sub lhs rhs) (int $ -i)
-- rest are just propagations
accumulateAdditiveConsts (e                      , 0) = e
accumulateAdditiveConsts (e                      , i) = Add e (int i)

-- pull distributed consts
pullAdditiveConstFromExpr :: Expr -> (Expr, Int)
-- actual optimizations
pullAdditiveConstFromExpr (Negate expr) =
    let (exprPartitioned, i) = pullAdditiveConstFromExpr expr in (Negate $ exprPartitioned, -i)
pullAdditiveConstFromExpr (Constr (ConstructInt i)) = (int 0, i)
pullAdditiveConstFromExpr (Constr (ConstructExpr expr)) =
    let (exprPartitioned, i) = pullAdditiveConstFromExpr expr in (Constr $ ConstructExpr exprPartitioned, i)
pullAdditiveConstFromExpr (Add lhs rhs) =
    let (lhsPartitioned, iL) = pullAdditiveConstFromExpr lhs
        (rhsPartitioned, iR) = pullAdditiveConstFromExpr rhs
    in  case (lhsPartitioned, rhsPartitioned) of
            (Constr (ConstructInt l), Constr (ConstructInt r)) -> (int (l + r + iL + iR), 0)
            (lhsFolded, Constr (ConstructInt 0)) -> (lhsFolded, iL + iR)
            (Constr (ConstructInt 0), rhsFolded) -> (rhsFolded, iL + iR)
            _ -> (Add lhsPartitioned rhsPartitioned, iL + iR)
pullAdditiveConstFromExpr (Sub lhs rhs) =
    let (lhsPartitioned, iL) = pullAdditiveConstFromExpr lhs
        (rhsPartitioned, iR) = pullAdditiveConstFromExpr rhs
    in  case (lhsPartitioned, rhsPartitioned) of
            (Constr (ConstructInt l), Constr (ConstructInt r)) -> (int (l + iL - r - iR), 0)
            (lhsFolded, Constr (ConstructInt 0)) -> (lhsFolded, iL - iR)
            (Constr (ConstructInt 0), rhsFolded) -> (Negate $ rhsFolded, iL - iR)
            _ -> (Sub lhsPartitioned rhsPartitioned, iL - iR)
-- rest are just propagations
pullAdditiveConstFromExpr e = (e, 0)
