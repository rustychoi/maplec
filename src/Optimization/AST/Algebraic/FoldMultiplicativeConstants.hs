module Optimization.AST.Algebraic.FoldMultiplicativeConstants
    ( foldMultiplicativeConsts
    ) where

import           AST                            ( Expr(..)
                                                , Constructor(..)
                                                )
import           Optimization.AST.Util          ( propagateExprOptim2Expr )

--
int :: Int -> Expr
int = Constr . ConstructInt

--
foldMultiplicativeConsts :: Expr -> Expr
foldMultiplicativeConsts (Negate expr                ) = folder $ Negate expr
foldMultiplicativeConsts (Mul lhs rhs                ) = folder $ Mul lhs rhs
foldMultiplicativeConsts (Div lhs rhs                ) = folder $ Div lhs rhs
foldMultiplicativeConsts (Constr (ConstructInt  i   )) = folder $ int i
foldMultiplicativeConsts (Constr (ConstructExpr expr)) = folder $ Constr $ ConstructExpr expr
foldMultiplicativeConsts expr                          = propagateExprOptim2Expr foldMultiplicativeConsts expr

--
folder :: Expr -> Expr
folder = accumulateMultiplicativeConsts . pullMultiplicativeConstFromExpr

-- helper for `foldConstExpr`
--
-- consumes the output of `pullMultiplicativeConstFromExpr`
accumulateMultiplicativeConsts :: (Expr, Int) -> Expr
-- actual optimizations
accumulateMultiplicativeConsts (Constr (ConstructInt n), i) = int (n * i)
accumulateMultiplicativeConsts (Mul lhs rhs            , i) = Mul (Mul lhs rhs) (int i)
accumulateMultiplicativeConsts (Div lhs rhs            , i) = Div (Div lhs rhs) (int i)
-- rest are just propagations
accumulateMultiplicativeConsts (e                      , 0) = int 0
accumulateMultiplicativeConsts (e                      , 1) = e
accumulateMultiplicativeConsts (e                      , i) = Mul e (int i)

-- pull consts
pullMultiplicativeConstFromExpr :: Expr -> (Expr, Int)
-- actual optimizations
pullMultiplicativeConstFromExpr (Negate expr) =
    let (exprPartitioned, i) = pullMultiplicativeConstFromExpr expr
    in  case i of
            1  -> (Negate exprPartitioned, 1)
            -1 -> (exprPartitioned, 1)
            _  -> (exprPartitioned, -i)
pullMultiplicativeConstFromExpr (Constr (ConstructInt 0)) = (int 0, 0)
pullMultiplicativeConstFromExpr (Constr (ConstructInt i)) = (int 1, i)
pullMultiplicativeConstFromExpr (Constr (ConstructExpr expr)) =
    let (exprPartitioned, i) = pullMultiplicativeConstFromExpr expr in (Constr $ ConstructExpr exprPartitioned, i)
pullMultiplicativeConstFromExpr (Mul lhs rhs) =
    let (lhsPartitioned, iL) = pullMultiplicativeConstFromExpr lhs
        (rhsPartitioned, iR) = pullMultiplicativeConstFromExpr rhs
    in  case (lhsPartitioned, rhsPartitioned) of
            (Constr (ConstructInt l), Constr (ConstructInt r)) -> (int (l * r * iL * iR), 1)
            (lhsFolded, Constr (ConstructInt 0)) -> (int 0, 0)
            (lhsFolded, Constr (ConstructInt 1)) -> (lhsFolded, iL * iR)
            (Constr (ConstructInt 0), rhsFolded) -> (int 0, 0)
            (Constr (ConstructInt 1), rhsFolded) -> (rhsFolded, iL * iR)
            _ -> (Mul lhsPartitioned rhsPartitioned, iL * iR)
pullMultiplicativeConstFromExpr (Div lhs rhs) =
    let (lhsPartitioned, iL) = pullMultiplicativeConstFromExpr lhs
        (rhsPartitioned, iR) = pullMultiplicativeConstFromExpr rhs
    in  case (lhsPartitioned, rhsPartitioned) of
            (Constr (ConstructInt l), Constr (ConstructInt r)) -> (int (l * iL `quot` r `quot` iR), 1)
            (lhsFolded, Constr (ConstructInt 0)) -> error "undefined, make a variant for undef for warning"
            (lhsFolded, Constr (ConstructInt 1)) -> (lhsFolded, iL `quot` iR)
            (Constr (ConstructInt 0), rhsFolded) -> (int 0, 0)
            (Constr (ConstructInt 1), rhsFolded) -> (Div (int 1) rhsFolded, iL `quot` iR)
            _ -> (Div lhsPartitioned rhsPartitioned, iL `quot` iR)
-- rest are just propagations
pullMultiplicativeConstFromExpr e = (e, 1)
