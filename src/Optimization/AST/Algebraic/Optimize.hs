module Optimization.AST.Algebraic.Optimize
    ( optimizeExprAlgebraically
    ) where

import           AST                            ( Expr )
import           Optimization.AST.Algebraic.ConstantFold
                                                ( constantFoldExpr )

-- chain using .
optimizeExprAlgebraically :: Expr -> Expr
optimizeExprAlgebraically = constantFoldExpr
