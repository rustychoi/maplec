module Optimization.AST.Optimize
    ( optimizeProg
    ) where

import           AST                            ( Expr(..)
                                                , Deconstructor(..)
                                                , Constructor(..)
                                                , Prog(..)
                                                , Statement(..)
                                                )
import           Optimization.AST.Algebraic.Optimize
                                                ( optimizeExprAlgebraically )
import           Optimization.AST.Util          ( makeLegible )

-- chain using .
optimizeExpr :: Expr -> Expr
optimizeExpr = makeLegible . optimizeExprAlgebraically

--
optimizeProg :: Prog -> Prog
optimizeProg (Program stmts expr) = Program (map optimizeStatement stmts) (optimizeExpr expr)

--
optimizeStatement :: Statement -> Statement
optimizeStatement (Decl name args expr) = Decl name args (optimizeExpr expr)
optimizeStatement s                     = s

--
optimizeConstr :: Constructor -> Constructor
optimizeConstr (ConstructListCons head tail) =
    let headOptimized = optimizeConstr head
        tailOptimized = optimizeConstr tail
    in  ConstructListCons headOptimized tailOptimized
optimizeConstr (ConstructData name args) = ConstructData name (map optimizeConstr args)
optimizeConstr (ConstructExpr expr     ) = ConstructExpr $ optimizeExpr expr
optimizeConstr c                         = c
