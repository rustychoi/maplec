module AST
    ( Prog(..)
    , Statement(..)
    , Expr(..)
    , TypeDeclaration(..)
    , Constructor(..)
    , Deconstructor(..)
    ) where

import           Data.List

--
data Prog = Program [Statement] Expr
    deriving (Eq, Show)

--
data TypeDeclaration = FuncTypeDeclaration String TypeDeclaration [TypeDeclaration]
                     | DataTypeDeclaration String [TypeDeclaration]
    deriving (Eq, Show)

--
data Statement = Decl String [Deconstructor] Expr
          | TypeDecl TypeDeclaration
          | DataDecl String [String] (String, [String]) [(String, [String])]
    deriving (Eq, Show)

--
data Constructor = ConstructInt Int
                 | ConstructBool Bool
                 | ConstructStr String
                 | ConstructChar Char
                 | ConstructValue String
                 | ConstructListNil
                 | ConstructListCons Constructor Constructor
                 | ConstructExpr Expr
                 | ConstructData String [Constructor]
    deriving (Eq, Show)
-- almost the same as constructor, except
-- that we don't allow arbitrary expressions, and it's subbed out with DeconstructValue
data Deconstructor = DeconstructValue String
                   | DeconstructListCons Deconstructor Deconstructor
                   | DeconstructListNil
                   | DeconstructBool Bool
                   | DeconstructInt Int
                   | DeconstructStr String
                   | DeconstructChar Char
                   | DeconstructData String [Deconstructor]
    deriving (Eq, Show)

--
data Expr = Add Expr Expr
          | Sub Expr Expr
          | Mul Expr Expr
          | Div Expr Expr
          | Mod Expr Expr
          | Negate Expr
          | And Expr Expr
          | Or Expr Expr
          | Eq Expr Expr
          | Neq Expr Expr
          | Gt Expr Expr
          | Ge Expr Expr
          | Lt Expr Expr
          | Le Expr Expr
          | Not Expr
          | IfElse Expr Expr Expr
          | Call Expr Expr
          | Lambda Deconstructor Expr
          | LetIn [Statement] Expr
          | Constr Constructor
          | Match Expr (Deconstructor, Expr) [(Deconstructor, Expr)]
    deriving (Eq, Show)
