module Lib
    ( someFunc
    ) where

import           AST                            ( Expr(..)
                                                , Prog(..)
                                                , Statement(..)
                                                )

someFunc :: IO ()
someFunc = putStrLn "maplec!"
