module TestUtil
    ( val
    , int
    , typedInt
    ) where

import           Data.List
import           Test.Hspec
import           Test.QuickCheck
import           Optimization.AST.Optimize      ( optimizeProg )
import           AST                            ( Prog(..)
                                                , Statement(..)
                                                , Expr(..)
                                                , Constructor(..)
                                                , Deconstructor(..)
                                                )
import           Type.TypedAST                  ( TExpr(..)
                                                , TypedConstructor(..)
                                                , TypedDeconstructor(..)
                                                , TypedExpr(..)
                                                , TypedProgram(..)
                                                , TypedStatement(..)
                                                , TPrimitive(..)
                                                )
val :: String -> Expr
val = Constr . ConstructValue

int :: Int -> Expr
int = Constr . ConstructInt

typedInt :: Int -> TypedExpr
typedInt = TypedConstr . TypedConstructInt
