module Optimization.Spec
    ( specOptimize
    ) where

import           Test.Hspec
import           Test.QuickCheck
import           Optimization.AST.Spec          ( specOptimizeAST )

--
specOptimize = describe "Optimization" $ do
    _ <- specOptimizeAST
    return ()
