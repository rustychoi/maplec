module Optimization.AST.Algebraic.Spec
    ( specOptimizeASTAlgebraic
    ) where

import           Data.List
import           Test.Hspec
import           Test.QuickCheck
import           Optimization.AST.Optimize      ( optimizeProg )
import           AST                            ( Prog(..)
                                                , Statement(..)
                                                , Expr(..)
                                                , Constructor(..)
                                                , Deconstructor(..)
                                                )
import           TestUtil                       ( val
                                                , int
                                                )
import           Optimization.AST.Algebraic.SpecAdditiveConstants
                                                ( specOptimizeASTAlgebraicAdditiveConstants )
import           Optimization.AST.Algebraic.SpecMultiplicativeConstants
                                                ( specOptimizeASTAlgebraicMultiplicativeConstants )
import           Optimization.AST.Algebraic.SpecRedundantNegation
                                                ( specOptimizeASTAlgebraicRedundantNegation )

--
specOptimizeASTAlgebraic = describe "Optimization.AST.Algebraic" $ do
    _ <- specOptimizeASTAlgebraicRedundantNegation
    _ <- specOptimizeASTAlgebraicAdditiveConstants
    _ <- specOptimizeASTAlgebraicMultiplicativeConstants
    return ()
