module Optimization.AST.Algebraic.SpecAdditiveConstants
    ( specOptimizeASTAlgebraicAdditiveConstants
    ) where

import           Data.List
import           Test.Hspec
import           Test.QuickCheck
import           Optimization.AST.Optimize      ( optimizeProg )
import           AST                            ( Prog(..)
                                                , Statement(..)
                                                , Expr(..)
                                                , Constructor(..)
                                                , Deconstructor(..)
                                                )
import           TestUtil                       ( val
                                                , int
                                                )


--
specOptimizeASTAlgebraicAdditiveConstants = describe "Optimization.AST.Algebraic fold additive constants" $ do
    _ <- specAddingConsts
    _ <- specNestedConsts1
    _ <- specNestedConsts2
    _ <- specNestedWithVar
    _ <- specNestedWithVarManyConsts
    _ <- specNestedWithVarMult
    _ <- specZeroOnTheLeft
    _ <- specZeroOnTheRight
    return ()

--
specAddingConsts = it "adding consts" $ do
    testAddingConsts
--
testAddingConsts =
    let result   = optimizeProg $ Program [] inputExpr where inputExpr = Add (int 6) (int 2)
        expected = Program [] outputExpr where outputExpr = int 8
    in  result `shouldBe` expected
--
specZeroOnTheLeft = it "zero on the left" $ do
    testZeroOnTheLeft
--
testZeroOnTheLeft =
    let result   = optimizeProg $ Program [] inputExpr where inputExpr = Add (int 0) (int 2)
        expected = Program [] outputExpr where outputExpr = int 2
    in  result `shouldBe` expected
--
specZeroOnTheRight = it "zero on the right" $ do
    testZeroOnTheRight
--
testZeroOnTheRight =
    let result   = optimizeProg $ Program [] inputExpr where inputExpr = Add (int 3) (int 0)
        expected = Program [] outputExpr where outputExpr = int 3
    in  result `shouldBe` expected
--
specNestedConsts1 = it "nested consts 1" $ do
    testNestedConsts1
--
testNestedConsts1 =
    let result   = optimizeProg $ Program [] inputExpr where inputExpr = Add (Sub (int 8) (int 8)) (int 2)
        expected = Program [] outputExpr where outputExpr = int 2
    in  result `shouldBe` expected
--
specNestedConsts2 = it "nested consts 2" $ do
    testNestedConsts2
--
testNestedConsts2 =
    let result   = optimizeProg $ Program [] inputExpr where inputExpr = Sub (Sub (int 8) (int 19)) (int 2)
        expected = Program [] outputExpr where outputExpr = int $ -13
    in  result `shouldBe` expected
--
specNestedWithVar = it "nested with var" $ do
    testNestedWithVar
--
testNestedWithVar =
    let result   = optimizeProg $ Program [] inputExpr where inputExpr = Add (Sub (val "x") (int 8)) (int 2)
        expected = Program [] outputExpr where outputExpr = Sub (val "x") (int 6)
    in  result `shouldBe` expected
--
specNestedWithVarMult = it "nested with var, mult" $ do
    testNestedWithVarMult
--
testNestedWithVarMult =
    let result = optimizeProg $ Program [] inputExpr              where
                inputExpr = Add lhs (int 2)
                lhs       = Sub (val "x") (Add (int 5) (Mul (val "y") (int 32)))
        expected = Program [] $ outputExpr              where
                outputExpr = Sub lhs (int 3)
                lhs        = Sub (val "x") (Mul (int 32) (val "y"))
    in  result `shouldBe` expected
--
specNestedWithVarManyConsts = it "nested with var, many consts" $ do
    testNestedWithVarManyConsts
--
testNestedWithVarManyConsts =
    let result   = optimizeProg $ Program [] inputExpr where inputExpr = Add (Add (int 3) (Sub (val "x") (int 39))) (int 2)
        expected = Program [] $ outputExpr where outputExpr = Sub (val "x") (int 34)
    in  result `shouldBe` expected
