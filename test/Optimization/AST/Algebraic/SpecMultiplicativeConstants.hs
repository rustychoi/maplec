module Optimization.AST.Algebraic.SpecMultiplicativeConstants
    ( specOptimizeASTAlgebraicMultiplicativeConstants
    ) where

import           Data.List
import           Test.Hspec
import           Test.QuickCheck
import           Optimization.AST.Optimize      ( optimizeProg )
import           AST                            ( Prog(..)
                                                , Statement(..)
                                                , Expr(..)
                                                , Constructor(..)
                                                , Deconstructor(..)
                                                )
import           TestUtil                       ( val
                                                , int
                                                )

-- TODO decide on the semantics of div optimizing
specOptimizeASTAlgebraicMultiplicativeConstants = describe "Optimization.AST.Algebraic fold additive constants" $ do
    _ <- specMultiplyConsts
    _ <- specMultiplyConstsOne
    _ <- specMultiplyConstsZero
    _ <- specMultiplyConstsZero2
    _ <- specMultiplyConstsOne2
    _ <- specMultiplyConstsValZero
    _ <- specMultiplyConstsNestedZero
    return ()

----------------------------------------------------------------

--
specMultiplyConsts = it "multiplying consts" $ do
    testMultiplyConsts
--
testMultiplyConsts =
    let inputExpr = Mul (int 6) (int 2)
        result    = optimizeProg $ Program [] inputExpr
        expected  = Program [] outputExpr where outputExpr = int 12
    in  result `shouldBe` expected

----------------------------------------------------------------

--
specMultiplyConstsOne = it "multiplying one" $ do
    testMultiplyConstsOne
--
testMultiplyConstsOne =
    let inputExpr = Mul (int 739) (int 1)
        result    = optimizeProg $ Program [] inputExpr
        expected  = Program [] outputExpr where outputExpr = int 739
    in  result `shouldBe` expected

----------------------------------------------------------------

--
specMultiplyConstsZero = it "multiplying consts zero" $ do
    testMultiplyConstsZero
--
testMultiplyConstsZero =
    let inputExpr = Mul (int 0) (int 2)
        result    = optimizeProg $ Program [] inputExpr
        expected  = Program [] outputExpr where outputExpr = int 0
    in  result `shouldBe` expected

----------------------------------------------------------------

--
specMultiplyConstsZero2 = it "multiplying consts zero 2" $ do
    testMultiplyConstsZero2
--
testMultiplyConstsZero2 =
    let inputExpr = Mul (int 14) (int 0)
        result    = optimizeProg $ Program [] inputExpr
        expected  = Program [] outputExpr where outputExpr = int 0
    in  result `shouldBe` expected

----------------------------------------------------------------

--
specMultiplyConstsOne2 = it "multiplying val identity" $ do
    testMultiplyConstsOne2
--
testMultiplyConstsOne2 =
    let inputExpr = Mul (val "x") (int 1)
        result    = optimizeProg $ Program [] inputExpr
        expected  = Program [] outputExpr where outputExpr = val "x"
    in  result `shouldBe` expected

----------------------------------------------------------------

--
specMultiplyConstsValZero = it "multiplying val zero" $ do
    testMultiplyConstsValZero
--
testMultiplyConstsValZero =
    let inputExpr = Mul (int 0) (val "x")
        result    = optimizeProg $ Program [] inputExpr
        expected  = Program [] outputExpr where outputExpr = int 0
    in  result `shouldBe` expected

----------------------------------------------------------------

--
specMultiplyConstsNestedZero = it "nested zero" $ do
    testMultiplyConstsNestedZero
--
testMultiplyConstsNestedZero =
    let inputExpr = Mul (Mul (val "x") (Mul (val "y") (int 829))) (Sub (int 439) (int 439))
        result    = optimizeProg $ Program [] inputExpr
        expected  = Program [] outputExpr where outputExpr = int 0
    in  result `shouldBe` expected
