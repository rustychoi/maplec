module Optimization.AST.Algebraic.SpecRedundantNegation
    ( specOptimizeASTAlgebraicRedundantNegation
    ) where

import           Data.List
import           Test.Hspec
import           Test.QuickCheck
import           Optimization.AST.Optimize      ( optimizeProg )
import           AST                            ( Prog(..)
                                                , Statement(..)
                                                , Expr(..)
                                                , Constructor(..)
                                                , Deconstructor(..)
                                                )
import           TestUtil                       ( val
                                                , int
                                                )
--
specOptimizeASTAlgebraicRedundantNegation = describe "optimize redundant negation" $ do
    _ <- specNoRedundantNegation
    _ <- specDoubleNegation
    _ <- specTripleNegation
    _ <- specQuadNegation
    _ <- specQuintNegation
    _ <- specNegatePositiveInt
    _ <- specNegateNegativeInt
    _ <- specNegatePositiveIntTwice
    return ()

----------------------------------------------------------------

--
specNoRedundantNegation = it "no redundant negation" $ do
    testNoRedundantNegation
--
testNoRedundantNegation =
    let result   = optimizeProg $ Program [] inputExpr where inputExpr = int 3
        expected = Program [] outputExpr where outputExpr = int 3
    in  result `shouldBe` expected

----------------------------------------------------------------

--
specDoubleNegation = it "double negation" $ do
    testDoubleNegation
--
testDoubleNegation =
    let result   = optimizeProg $ Program [] inputExpr where inputExpr = Negate $ Negate $ val "x"
        expected = Program [] outputExpr where outputExpr = val "x"
    in  result `shouldBe` expected

----------------------------------------------------------------

--
specTripleNegation = it "triple negation" $ do
    testTripleNegation
--
testTripleNegation =
    let result   = optimizeProg $ Program [] inputExpr where inputExpr = Negate $ Negate $ Negate $ val "x"
        expected = Program [] outputExpr where outputExpr = Negate $ val "x"
    in  result `shouldBe` expected

----------------------------------------------------------------

--
specQuadNegation = it "quad negation" $ do
    testQuadNegation
--
testQuadNegation =
    let result   = optimizeProg $ Program [] inputExpr where inputExpr = Negate $ Negate $ Negate $ Negate $ val "x"
        expected = Program [] outputExpr where outputExpr = val "x"
    in  result `shouldBe` expected

----------------------------------------------------------------

--
specQuintNegation = it "quint negation" $ do
    testQuintNegation
--
testQuintNegation =
    let result   = optimizeProg $ Program [] inputExpr where inputExpr = Negate $ Negate $ Negate $ Negate $ Negate $ val "x"
        expected = Program [] outputExpr where outputExpr = Negate $ val "x"
    in  result `shouldBe` expected

----------------------------------------------------------------

--
specNegatePositiveInt = it "negate positive int" $ do
    testNegatePositiveInt
--
testNegatePositiveInt =
    let result   = optimizeProg $ Program [] inputExpr where inputExpr = Negate $ int 3
        expected = Program [] outputExpr where outputExpr = int (-3)
    in  result `shouldBe` expected

----------------------------------------------------------------

--
specNegateNegativeInt = it "negate negative int" $ do
    testNegateNegativeInt
--
testNegateNegativeInt =
    let result   = optimizeProg $ Program [] inputExpr where inputExpr = Negate $ int (-8)
        expected = Program [] outputExpr where outputExpr = int 8
    in  result `shouldBe` expected

----------------------------------------------------------------

--
specNegatePositiveIntTwice = it "negate positive int twice" $ do
    testNegatePositiveIntTwice
--
testNegatePositiveIntTwice =
    let result   = optimizeProg $ Program [] inputExpr where inputExpr = Negate $ Negate $ int 8
        expected = Program [] outputExpr where outputExpr = int 8
    in  result `shouldBe` expected
