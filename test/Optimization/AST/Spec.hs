module Optimization.AST.Spec
    ( specOptimizeAST
    ) where

import           Test.Hspec
import           Test.QuickCheck
import           Optimization.AST.Algebraic.Spec
                                                ( specOptimizeASTAlgebraic )

--
specOptimizeAST = describe "Optimization.AST" $ do
    _ <- specOptimizeASTAlgebraic
    return ()
