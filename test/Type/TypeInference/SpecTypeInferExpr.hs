module Type.TypeInference.SpecTypeInferExpr
    ( specTypeInferExpr
    ) where

import           Test.Hspec
import           Test.QuickCheck
import           Type.TypedAST                  ( TExpr(..)
                                                , TypedConstructor(..)
                                                , TypedDeconstructor(..)
                                                , TypedExpr(..)
                                                , TypedProgram(..)
                                                , TypedStatement(..)
                                                , TPrimitive(..)
                                                )
import           Type.TypeInference             ( typeInfer )
import           Type.Internal.Constraint       ( constraints
                                                , Constraint(..)
                                                )
import           Type.Internal.ConstraintSolver ( solveConstraints )
import           AST                            ( Prog(..)
                                                , Statement(..)
                                                , Expr(..)
                                                , Constructor(..)
                                                , Deconstructor(..)
                                                )
import           TestUtil                       ( val
                                                , int
                                                , typedInt
                                                )
import           Data.Map                       ( Map
                                                , empty
                                                )

-- NOTE for some example if we inline things into `do`, britanny fails to format the code
-- NOTE likewise for nesting more than 1 level of `do`
specTypeInferExpr = describe "Type.TypeInference.SpecTypeInferExpr" $ do
    _ <- specAddValToConst
    _ <- specAddTwoVals
    _ <- specIsEven
    _ <- specIntAsHead
    _ <- specBoolAsHead
    return ()

----------------------------------------------------------------

typeInt :: TExpr
typeInt = TPrim PrimInt

typeBool :: TExpr
typeBool = TPrim PrimBool

----------------------------------------------------------------

-- infer the types of the following:
--     \x -> x + 8
--
-- should be
--     :: Int -> Int
specAddValToConst = it "\\x -> x + 8 :: Int -> Int" $ do
    testAddValToConst
--
testAddValToConst =
    let --
        result = typeInfer sourceAST              where
                -- \x -> x + 8
                sourceAST = Program statements programBody                  where
                    statements  = []
                    programBody = Lambda arg body                      where
                        arg  = DeconstructValue "x"
                        body = Add (val "x") (int 8)
        --
        expected = TypedProg statements programBody              where
                statements  = []
                programBody = TypedLambda funcType arg body                  where
                    funcType = TFunc typeInt typeInt
                    arg      = TypedDeconstructValue typeInt "x"
                    body     = TypedAdd lhs rhs                      where
                        lhs = TypedConstr (TypedConstructValue typeInt "x")
                        rhs = TypedConstr (TypedConstructInt 8)
    --
    in  result `shouldBe` expected

----------------------------------------------------------------

-- infer the types of the following:
--     \x y -> x + y
--
-- should be
--     :: Int -> Int -> Int
specAddTwoVals = it "\\x y -> x + y :: Int -> Int :: Int" $ do
    testAddTwoVals
--
testAddTwoVals =
    let --
        result = typeInfer sourceAST              where
                -- \x y -> x + y
                sourceAST = Program statements programBody                  where
                    statements  = []
                    programBody = Lambda arg0 body0                      where
                        arg0  = DeconstructValue "x"
                        body0 = Lambda arg1 body1                          where
                            arg1  = DeconstructValue "y"
                            body1 = Add (val "x") (val "y")
        --
        expected = TypedProg statements programBody              where
                statements  = []
                programBody = TypedLambda funcType0 arg0 body0                  where
                    funcType0 = TFunc typeInt (TFunc typeInt typeInt)
                    arg0      = TypedDeconstructValue typeInt "x"
                    body0     = TypedLambda funcType1 arg1 body1                      where
                        funcType1 = TFunc typeInt typeInt
                        arg1      = TypedDeconstructValue typeInt "y"
                        body1     = TypedAdd lhs rhs                          where
                            lhs = TypedConstr (TypedConstructValue typeInt "x")
                            rhs = TypedConstr (TypedConstructValue typeInt "y")
    --
    in  result `shouldBe` expected

----------------------------------------------------------------

-- infer the types of the following:
--     \x -> x % 2 == 0
--
-- should be
--     :: Int -> Bool
specIsEven = it "\\x -> x % 2 == 0 :: Int -> Bool" $ do
    testIsEven
--
testIsEven =
    let --
        result = typeInfer sourceAST              where
                -- \x -> x + 8
                sourceAST = Program statements programBody                  where
                    statements  = []
                    programBody = Lambda arg body                      where
                        arg  = DeconstructValue "x"
                        body = Eq lhs rhs                          where
                            lhs = Mod (val "x") (int 2)
                            rhs = int 0
        --
        expected = TypedProg statements programBody              where
                statements  = []
                programBody = TypedLambda funcType arg body                  where
                    funcType = TFunc typeInt typeBool
                    arg      = TypedDeconstructValue typeInt "x"
                    body     = TypedEq lhs0 rhs                      where
                        lhs0 = TypedMod lhs1 (typedInt 2) where lhs1 = TypedConstr (TypedConstructValue typeInt "x")
                        rhs  = typedInt 0
    --
    in  result `shouldBe` expected

----------------------------------------------------------------

-- infer the types of the following:
--     \xs -> 0 : xs
--
-- should be
--     :: List Int -> List Int
specIntAsHead = it "\\xs -> 0 : xs :: List Int -> List Int" $ do
    testIntAsHead
--
testIntAsHead =
    let --
        result = typeInfer sourceAST          where
                -- \xs -> 0 : xs
            sourceAST = Program statements programBody              where
                statements  = []
                programBody = Lambda arg body                  where
                    arg  = DeconstructValue "xs"
                    body = Constr $ ConstructListCons (ConstructInt 0) (ConstructValue "xs")
        --
        expected = TypedProg statements programBody          where
            statements  = []
            programBody = TypedLambda funcType arg body              where
                funcType = TFunc (TList typeInt) (TList typeInt)
                arg      = TypedDeconstructValue (TList typeInt) "xs"
                body     = TypedConstr $ TypedConstructListCons (TList typeInt)
                                                                (TypedConstructInt 0)
                                                                (TypedConstructValue (TList typeInt) "xs")
    --
    in
        result `shouldBe` expected

----------------------------------------------------------------

-- infer the types of the following:
--     \xs -> True : xs
--
-- should be
--     :: List Bool -> List Bool
specBoolAsHead = it "\\xs -> True : xs :: List Bool -> List Bool" $ do
    testBoolAsHead
--
testBoolAsHead =
    let --
        result = typeInfer sourceAST          where
                -- \xs -> 0 : xs
            sourceAST = Program statements programBody              where
                statements  = []
                programBody = Lambda arg body                  where
                    arg  = DeconstructValue "xs"
                    body = Constr $ ConstructListCons (ConstructBool True) (ConstructValue "xs")
        --
        expected = TypedProg statements programBody          where
            statements  = []
            programBody = TypedLambda funcType arg body              where
                funcType = TFunc (TList typeBool) (TList typeBool)
                arg      = TypedDeconstructValue (TList typeBool) "xs"
                body     = TypedConstr $ TypedConstructListCons (TList typeBool)
                                                                (TypedConstructBool True)
                                                                (TypedConstructValue (TList typeBool) "xs")
    --
    in
        result `shouldBe` expected
