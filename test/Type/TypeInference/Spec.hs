module Type.TypeInference.Spec
    ( specTypeInference
    ) where

import           Test.Hspec
import           Test.QuickCheck
import           Type.TypeInference.SpecTypeInferExpr
                                                ( specTypeInferExpr )

--
specTypeInference = describe "Type.TypeInference" $ do
    _ <- specTypeInferExpr
    return ()
