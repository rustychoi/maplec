module Type.Spec
    ( specType
    ) where

import           Test.Hspec
import           Test.QuickCheck
import           Type.TypeInference.Spec        ( specTypeInference )

--
specType = describe "Type" $ do
    _ <- specTypeInference
    return ()
