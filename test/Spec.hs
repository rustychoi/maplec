import           Test.Hspec
import           Test.QuickCheck
import           Optimization.Spec              ( specOptimize )
import           Type.Spec                      ( specType )

--
main :: IO ()
main = hspec $ do
    _ <- specOptimize
    _ <- specType
    return ()
